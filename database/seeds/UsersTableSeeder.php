<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'name' => 'Victor Hugo',
            'email' => 'victorhsanjos@gmail.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
