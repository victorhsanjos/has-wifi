<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('foods')->insert([
            ['name' =>  'Doce de batata doce'],
            ['name' =>  'Churrasco'],
            ['name' =>  'Bala de banana Oliveira ou similares'],
            ['name' =>  'Tapioca'],
            ['name' =>  'Pizza assado no forno à lenha'],
            ['name' =>  'Feijão tropeiro'],
            ['name' =>  'Arroz carreteiro'],
            ['name' =>  'Açaí na tijela'],
            ['name' =>  'Paçoca de amendoim'],
            ['name' =>  'Pato no tucupi'],
            ['name' =>  'Maniçoba'],
            ['name' =>  'Baião de dois'],
            ['name' =>  'Acarajé'],
            ['name' =>  'Pamonha'],
            ['name' =>  'Dobradinha'],
            ['name' =>  'Rapadura'],
            ['name' =>  'Farofa de içá'],
            ['name' =>  'Barreado'],
            ['name' =>  'Pastel de feira'],
            ['name' =>  'Couve refogada com alho'],
            ['name' =>  'Sanduíche de pernil'],
            ['name' =>  'Palmito'],
            ['name' =>  'Umbu em natura'],
            ['name' =>  'Pacu'],
            ['name' =>  'Camarão na moranga'],
            ['name' =>  'Doce de abóbora'],
            ['name' =>  'Feijoada'],
            ['name' =>  'Galinhada com pequi'],
            ['name' =>  'Peixe na telha'],
            ['name' =>  'Biscoito de polvilho'],
            ['name' =>  'Galinha à cabidela'],
            ['name' =>  'Pão de mel com doce de leite'],
            ['name' =>  'Algum peixe assado na folha de bananeira'],
            ['name' =>  'Queijo coalho na brasa'],
            ['name' =>  'Curau'],
            ['name' =>  'Torta de liquidicador'],
            ['name' =>  'Café coado no filtro de pano'],
            ['name' =>  'Caldo de cana'],
            ['name' =>  'Arroz, feijão, bife e batata frita'],
            ['name' =>  'Buchada de bode'],
            ['name' =>  'Bolo de rolo'],
            ['name' =>  'Furrundum'],
            ['name' =>  'Chá mate gelado'],
            ['name' =>  'Rabada'],
            ['name' =>  'Vaca atolada'],
            ['name' =>  'Pitanga'],
            ['name' =>  'Quibebe'],
            ['name' =>  'Pintando na brasa'],
            ['name' =>  'Cuscuz paulista'],
            ['name' =>  'Quebra queixo']
        ]);
    }
}
