<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DrinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('drinks')->insert([
            ['name' => 'Absinto'],
            ['name' => 'Cachaça'],
            ['name' => 'Gim'],
            ['name' => 'Rum'],
            ['name' => 'Uísque'],
            ['name' => 'Vodka'],
            ['name' => 'Cerveja'],
            ['name' => 'Vinho'],
            ['name' => 'Champanhe'],
            ['name' => 'Conhaque'],
            ['name' => 'Saquê'],
            ['name' => 'Tequila'],
        ]);
    }
}
