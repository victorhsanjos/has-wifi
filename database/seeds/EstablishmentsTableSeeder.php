<?php

use Illuminate\Database\Seeder;

class EstablishmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Establishment::class)
            ->create([
                'name' => 'Restaurante e Pizzaria Atlântico',
                'type_id' => 2,
            ])->addresses()->saveMany([
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Av. Fagundes Varela',
                    'number' => '111',
                    'district' => 'Jardim Atlântico',
                    'city' => 'Olinda',
                    'state' => 'Pernanmbuco',
                    'country' => 'Brasil',
                ]),
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Av. Rui Barbosa',
                    'number' => '500',
                    'district' => 'Graças',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ]),
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Av. Ayrton Senna da Silva',
                    'number' => '750',
                    'district' => 'Piedade',
                    'city' => 'Jaboatão dos Guararapes',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ]),
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Av. Gov. Agamenon Magalhães',
                    'number' => '153',
                    'district' => 'Santo Amaro',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ])
            ]);


        factory(App\Establishment::class)
            ->create([
                'name' => 'Livraria Cultura',
                'type_id' => '4',
            ])->addresses()->saveMany([
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Rua Senador Dantas',
                    'number' => '45',
                    'district' => 'Centro',
                    'city' => 'Rio de Janeiro',
                    'state' => 'Rio de Janeiro',
                    'country' => 'Brasil',
                ]),
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Avenida Paulista',
                    'number' => '2073',
                    'district' => 'Bela Vista',
                    'city' => 'São Paulo',
                    'state' => 'São Paulo',
                    'country' => 'Brasil',
                ]),
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Rua Madre de Deus',
                    'number' => 's/n',
                    'district' => 'Recife',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ]),
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Rua Brigadeiro Franco',
                    'number' => '2300',
                    'complement' => 'Piso 3 - Loja 306',
                    'district' => 'Centro',
                    'city' => 'Curitiba',
                    'state' => 'Paraná',
                    'country' => 'Brasil',
                ]),
            ]);

        factory(App\Establishment::class)
            ->create([
                'name' => 'São Braz',
                'type_id' => '1',
            ])->addresses()->saveMany([
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Rua Benfica',
                    'number' => '505',
                    'district' => 'Madalena',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ]),
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Avenida Paulista',
                    'number' => '2073',
                    'district' => 'Bela Vista',
                    'city' => 'São Paulo',
                    'state' => 'São Paulo',
                    'country' => 'Brasil',
                ]),
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Rua Madre de Deus',
                    'number' => 's/n',
                    'district' => 'Recife',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ])
            ]);

        factory(App\Establishment::class)
            ->create([
                'name' => 'Coworking Connection',
                'type_id' => '3',
            ])->addresses()->save(
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Rua Ana Angélica',
                    'number' => '64',
                    'district' => 'Derby',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ])
            );

        factory(App\Establishment::class)
            ->create([
                'name' => 'Workhall Coworking',
                'type_id' => '3',
            ])->addresses()->save(
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Av. Conselheiro Rosa e Silva',
                    'number' => '1460',
                    'complement' => 'Shopping ETC, Pavimento G',
                    'district' => 'Aflitos',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ])
            );

        factory(App\Establishment::class)
            ->create([
                'name' => 'Renor Office Coworking',
                'type_id' => 3
            ])->addresses()->save(
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Av. República do Líbano',
                    'number' => '251',
                    'complement' => 'Rio Mar Trade Center 3, Sala 2801',
                    'district' => 'Pina',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ])
            );

        factory(App\Establishment::class)
            ->create([
                'name' => 'Portal da Picanha',
                'type_id' => '2',
            ])->addresses()->save(
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'R. Dr. José Maria',
                    'number' => '911',
                    'district' => 'Rosarinho',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ])
            );

        factory(App\Establishment::class)
            ->create([
                'name' => 'O Rei da Picanha',
                'type_id' => '2',
            ])->addresses()->save(
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Av. Norte Miguel Arraes de Alencar',
                    'number' => '5100',
                    'district' => 'Casa Amarela',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ])
            );

        factory(App\Establishment::class)
            ->create([
                'name' => 'Genesis Culinária Japonesa',
                'type_id' => '2',
            ])->addresses()->save(
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Marques de Baipendi',
                    'number' => '122',
                    'district' => 'Campo Grande',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ])
            );

        factory(App\Establishment::class)
            ->create([
                'name' => 'Coni Mais',
                'type_id' => '2',
            ])->addresses()->save(
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'R. José Bonifácio',
                    'number' => '205',
                    'district' => 'Madalena',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ])
            );

        factory(App\Establishment::class)
            ->create([
                'name' => 'Bar do Agenor',
                'type_id' => '2',
            ])->addresses()->save(
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'R. Dom Manoel da Costa',
                    'number' => '595',
                    'district' => 'Torre',
                    'city' => 'Recife',
                    'state' => 'Pernambuco',
                    'country' => 'Brasil',
                ])
            );

        factory(App\Establishment::class)
            ->create([
                'name' => 'Pousada Penhasco',
                'type_id' => '5',
            ])->addresses()->save(
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Avenida Penhasco',
                    'number' => 's/n',
                    'district' => 'Bom Clima',
                    'city' => 'Chapada dos Guimarães',
                    'state' => 'Mato Grosso',
                    'country' => 'Brasil',
                ])
            );

        factory(App\Establishment::class)
            ->create([
                'name' => 'Pousada Quebra Mar',
                'type_id' => '5',
            ])->addresses()->save(
                factory(App\Address::class)->make([
                    'establishment_id' => null,
                    'street' => 'Praia dos Amores',
                    'number' => '220',
                    'district' => 'Barra da Tijuca',
                    'city' => 'Rio de Janeiro',
                    'state' => 'Rio de Janeiro',
                    'country' => 'Brasil',
                ])
            );
    }
}
