<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccommodationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accommodations')->insert([
            ['name' => 'Albergue'],
            ['name' => 'Apart-hotel'],
            ['name' => 'Campismo'],
            ['name' => 'Estalagem'],
            ['name' => 'Hotel'],
            ['name' => 'Hospedagem domiciliar'],
            ['name' => 'Hotel de charme'],
            ['name' => 'Hotel de lazer (ou Resort)'],
            ['name' => 'Hostel'],
            ['name' => 'Motel'],
            ['name' => 'Pensão'],
            ['name' => 'Pousada'],
            ['name' => 'Alojamento de turismo de habitação'],
            ['name' => 'Alojamento de turismo religioso'],
            ['name' => 'Alojamento de turismo rural'],
        ]);
    }
}
