<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AccommodationsTableSeeder::class);
        $this->call(DrinksTableSeeder::class);
        $this->call(EstablishmentTypesTableSeeder::class);
        $this->call(FoodsTableSeeder::class);
        $this->call(EstablishmentsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
