<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('address_id')->unsigned();
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->boolean('has_internet');
            $table->integer('internet_speed')->unsigned()->nullable();
            $table->boolean('is_open_internet')->nullable();
            $table->string('internet_password')->nullable();
            $table->integer('attendance_rating')->unsigned();
            $table->integer('price_rating')->unsigned();
            $table->integer('comfort_rating')->unsigned();
            $table->integer('noise_rating')->unsigned();
            $table->integer('overall_rating')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
