<?php

use Faker\Generator as Faker;

$factory->define(App\Establishment::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'type_id' => function () {
            return factory(App\EstablishmentType::class)->create()->id;
        }
    ];
});
