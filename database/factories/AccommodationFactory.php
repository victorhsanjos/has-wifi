<?php

use Faker\Generator as Faker;

$factory->define(App\Accommodation::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});
