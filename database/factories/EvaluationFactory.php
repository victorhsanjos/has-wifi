<?php

use Faker\Generator as Faker;

$factory->define(App\Evaluation::class, function (Faker $faker) {
    return [
        'address_id' => function() {
            return factory(App\Address::class)->create()->id;
        },
        'user_id' => function() {
            return factory(App\User::class)->create()->id;
        },
        'has_internet' => 1,
        'internet_speed' => $faker->randomDigitNotNull,
        'is_open_internet' => 1,
        'internet_password' => $faker->password,
        'attendance_rating' => $faker->numberBetween(1, 5),
        'price_rating' => $faker->numberBetween(1, 5),
        'comfort_rating' => $faker->numberBetween(1, 5),
        'noise_rating' => $faker->numberBetween(1, 5),
        'overall_rating' => $faker->numberBetween(1, 5),
    ];
});
