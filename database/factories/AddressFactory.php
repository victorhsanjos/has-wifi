<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    return [
        'establishment_id' => function () {
            return factory(App\Establishment::class)->create()->id;
        },
        'street' => $faker->streetName,
        'number' => $faker->randomNumber(4),
        'district' => $faker->word,
        'complement' => null,
        'city' => $faker->city,
        'state' => $faker->state,
        'country' => $faker->country,
    ];
});
