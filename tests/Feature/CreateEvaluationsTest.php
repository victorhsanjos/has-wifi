<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateEvaluationsTest extends TestCase
{
    use RefreshDatabase;

    private $address;

    protected function setUp()
    {
        parent::setUp();

        $this->address = factory(\App\Address::class)->create();
    }

    /** @test */
    public function unauthenticated_user_can_not_create_an_evaluation()
    {
        $this->get(route('evaluations.create', $this->address))->assertRedirect(route('login'));
        $this->post(route('evaluations.store', $this->address))->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_can_create_an_evaluation()
    {
        $this->withoutExceptionHandling();

        $this->actingAs(factory('App\User')->create());

        $evaluation = factory(\App\Evaluation::class)->make(['address_id' => null, 'user_id' => null]);
        $food = factory(\App\Food::class)->create();
        $drink = factory(\App\Drink::class)->create();
        $accommodation = factory(\App\Accommodation::class)->create();

        $data = array_diff_key($evaluation->toArray(), array_flip(['address_id', 'user_id']));
        $data = array_add($data, 'foods', [$food->id]);
        $data = array_add($data, 'drinks', [$drink->id]);
        $data = array_add($data, 'accommodations', [$accommodation->id]);

        $response = $this->post(route('evaluations.store', $this->address), $data);

        $response->assertRedirect(route('addresses.show', $this->address));
        $this->assertDatabaseHas('evaluations', array_diff_key($evaluation->toArray(), array_flip(['address_id', 'user_id'])));
        $this->assertDatabaseHas('evaluation_food', ['evaluation_id' => 1, 'food_id' => $food->id]);
        $this->assertDatabaseHas('drink_evaluation', ['drink_id' => $drink->id, 'evaluation_id' => 1]);
        $this->assertDatabaseHas('accommodation_evaluation', ['accommodation_id' => $accommodation->id, 'evaluation_id' => 1]);
    }

    /**
     * @test
     * @dataProvider emptyFieldsProvider
     */
    public function authenticated_user_can_not_create_an_evaluation_with_empty_fields($field, $custom_field, $value)
    {
        $this->actingAs(factory('App\User')->create());

        $data = [$field => $value];

        $response = $this->post(route('evaluations.store', $this->address), $data);

        $response->assertRedirect(route('evaluations.create', $this->address));
        $response->assertSessionHasErrors([$field => "The $custom_field field is required."]);
    }

    public function emptyFieldsProvider()
    {
        return [
            ['attendance_rating', 'attendance rating', ''],
            ['price_rating', 'price rating', ''],
            ['comfort_rating', 'comfort rating', ''],
            ['noise_rating', 'noise rating', ''],
            ['overall_rating', 'overall rating', ''],
        ];
    }

    /** @test */
    public function the_field_has_internet_must_be_boolean_to_create_an_evaluation()
    {
        $this->actingAs(factory('App\User')->create());

        $data = ['has_internet' => 'a'];

        $response = $this->post(route('evaluations.store', $this->address), $data);

        $response->assertRedirect(route('evaluations.create', $this->address));
        $response->assertSessionHasErrors(['has_internet' => 'The has internet field must be true or false.']);
    }

    /** @test */
    public function if_is_open_internet_is_false_then_the_internet_password_is_required_create_an_evaluation()
    {
        $this->actingAs(factory('App\User')->create());

        $data = [
            'has_internet' => 1,
            'is_open_internet' => 0,
        ];

        $response = $this->post(route('evaluations.store', $this->address), $data);

        $response->assertRedirect(route('evaluations.create', $this->address));
        $response->assertSessionHasErrors(['internet_password' => 'The internet password field is required.']);
    }

    /** @test */
    public function require_fields_if_has_internet_is_true_to_create_an_evaluation()
    {
        $this->actingAs(factory('App\User')->create());

        $data = [
            'has_internet' => 1,
            'internet_speed' => '',
        ];

        $response = $this->post(route('evaluations.store', $this->address), $data);

        $response->assertRedirect(route('evaluations.create', $this->address));
        $response->assertSessionHasErrors(['internet_speed' => 'The internet speed field is required when has internet is 1.']);
    }

    /**
     * @test
     * @dataProvider notExistFieldsProvider
     */
    public function the_fields_must_exist_in_the_database_to_create_an_evaluation($field, $custom_field, $value)
    {
        $this->actingAs(factory('App\User')->create());

        $data = [$field => $value];

        $response = $this->post(route('evaluations.store', $this->address), $data);

        $response->assertRedirect(route('evaluations.create', $this->address));
        $response->assertSessionHasErrors([$field => "The selected $custom_field is invalid."]);
    }

    public function notExistFieldsProvider()
    {
        return [
            ['foods', 'foods', '1'],
            ['drinks', 'drinks', '1'],
            ['accommodations', 'accommodations', '1'],
        ];
    }

    /**
     * @test
     * @dataProvider notIntergerFieldsProvider
     */
    public function fields_must_be_integer_to_create_an_evaluation($field, $custom_field, $value)
    {
        $this->actingAs(factory('App\User')->create());

        $data = [$field => $value];

        $response = $this->post(route('evaluations.store', $this->address), $data);

        $response->assertRedirect(route('evaluations.create', $this->address));
        $response->assertSessionHasErrors([$field => "The $custom_field must be an integer."]);
    }

    public function notIntergerFieldsProvider()
    {
        return [
            ['attendance_rating', 'attendance rating', 'a'],
            ['price_rating', 'price rating', 'vvvv'],
            ['comfort_rating', 'comfort rating', '@'],
            ['noise_rating', 'noise rating', '+'],
            ['overall_rating', 'overall rating', '?'],
        ];
    }

    /**
     * @test
     * @dataProvider notBetweenFieldsProvider
     */
    public function fields_must_be_between_1_and_5_to_create_an_evaluation($field, $custom_field, $value)
    {
        $this->actingAs(factory('App\User')->create());

        $data = [$field => $value];

        $response = $this->post(route('evaluations.store', $this->address), $data);

        $response->assertRedirect(route('evaluations.create', $this->address));
        $response->assertSessionHasErrors([$field => "The $custom_field must be between 1 and 5."]);
    }

    public function notBetweenFieldsProvider()
    {
        return [
            ['attendance_rating', 'attendance rating', '7'],
            ['price_rating', 'price rating', '8'],
            ['comfort_rating', 'comfort rating', '0'],
            ['noise_rating', 'noise rating', '21'],
            ['overall_rating', 'overall rating', '37'],
        ];
    }
}
