<?php

namespace Tests\Feature;

use App\Address;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateEstablishmentsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function unauthenticated_user_can_not_create_an_establishment()
    {
        $this->get(route('establishments.create'))->assertRedirect(route('login'));
        $this->get(route('establishments.store'))->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_can_create_an_establishment()
    {
        $this->withoutExceptionHandling();

        $this->actingAs(factory('App\User')->create());

        $establishment = factory('App\Establishment')->make();
        $address = factory('App\Address')->make(['establishment_id' => null]);

        $data = array_merge($establishment->toArray(), $address->toArray());

        $response = $this->post(route('establishments.store'), $data);

        $response->assertRedirect(route('evaluations.create', Address::first()));
        $this->assertDatabaseHas('establishments', $establishment->toArray());
        $this->assertDatabaseHas('addresses', array_except($address->toArray(), 'establishment_id'));
    }

    /** @test */
    public function authenticated_user_can_create_other_address_to_establishment()
    {
        $this->withoutExceptionHandling();

        $this->actingAs(factory('App\User')->create());

        $establishment = factory('App\Establishment')->create();
        $establishment->addresses()->save(factory(\App\Address::class)->make());

        $this->assertDatabaseHas('establishments', $establishment->toArray());
        $this->assertDatabaseHas('addresses', $establishment->addresses->first()->toArray());

        $otherAddress = factory('App\Address')->make(['establishment_id' => null]);

        $data = array_merge($establishment->toArray(), $otherAddress->toArray());

        $response = $this->post(route('establishments.store'), $data);

        $response->assertRedirect(route('evaluations.create', Address::find(2)));
        $this->assertDatabaseHas('establishments', array_except($establishment->toArray(), 'addresses'));
        $this->assertDatabaseHas('addresses', array_except($otherAddress->toArray(), 'establishment_id'));
        $this->assertCount(2, $establishment->refresh()->addresses);
    }

    /**
     * @test
     * @dataProvider emptyFieldsProvider
     */
    public function authenticated_user_can_not_create_an_establishment_with_empty_fields($field, $custom_field, $value)
    {
        $this->actingAs(factory('App\User')->create());

        $data = [$field => $value];

        $response = $this->post(route('establishments.store'), $data);

        $response->assertRedirect(route('establishments.create'));
        $response->assertSessionHasErrors([$field => "The $custom_field field is required."]);
    }

    public function emptyFieldsProvider()
    {
        return [
            ['name', 'name', ''],
            ['type_id', 'type', ''],
            ['street', 'street', ''],
            ['number', 'number', ''],
            ['city', 'city', ''],
            ['district', 'district', ''],
            ['state', 'state', ''],
            ['country', 'country', ''],
        ];
    }

    /**
     * @test
     * @dataProvider notExistFieldsProvider
     */
    public function the_fields_must_exist_in_the_database_to_create_an_establishment($field, $custom_field, $value)
    {
        $this->actingAs(factory('App\User')->create());

        $data = [$field => $value];

        $response = $this->post(route('establishments.store'), $data);

        $response->assertRedirect(route('establishments.create'));
        $response->assertSessionHasErrors([$field => "The selected $custom_field is invalid."]);
    }

    public function notExistFieldsProvider()
    {
        return [
            ['type_id', 'type', '1'],
        ];
    }
}
