#

## Requisitos

Nesse [link](https://laravel.com/docs/5.6/installation#server-requirements)
você encontra os requisitos necessários para rodar o projeto.

## Instalação

```
$ git clone https://victorhsanjos@bitbucket.org/victorhsanjos/has-wifi.git
```
Acesse o diretório do projeto e execute os seguintes comandos no **prompt**:

```
$ composer install
$ mv .env.example .env
$ php artisan key:generate
```

Em caso de dúvidas acesse o [link](https://laravel.com/docs/5.6/installation).

## Execução dos Testes

Navegue para o diretório do projeto e execute o seguinte comando no **prompt**:

```
$ ./vendor/bin/phpunit
```

Os testes estão localizado no pasta **tests/**.

## Revisão de Código

Foi utilizado o pacote [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) para verificar o código. A revisão foi realizada nos *models* do projeto. A documentação da ferramenta pode ser encontrada [aqui](https://github.com/squizlabs/PHP_CodeSniffer/wiki). O código revisado se encontra nos padrões recomendados do PHP ([PSR-1](https://www.php-fig.org/psr/psr-1/) e [PSR-2](https://www.php-fig.org/psr/psr-2/)).