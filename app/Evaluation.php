<?php
/**
 * Evaluation File
 *
 * PHP version 7
 *
 * @category Model
 * @package  App
 * @author   Victor Hugo <victorhsanjos@gmail.com>
 * @license  https://choosealicense.com/licenses/mit/ MIT License
 * @link     https://bitbucket.org/victorhsanjos/has-wifi/
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Evaluation Class
 *
 * @category Model
 * @package  App
 * @author   Victor Hugo <victorhsanjos@gmail.com>
 * @license  https://choosealicense.com/licenses/mit/ MIT License
 * @link     https://bitbucket.org/victorhsanjos/has-wifi/
 */
class Evaluation extends Model
{
    protected $fillable = [
        'address_id',
        'user_id',
        'has_internet',
        'internet_speed',
        'is_open_internet',
        'internet_password',
        'attendance_rating',
        'price_rating',
        'comfort_rating',
        'noise_rating',
        'overall_rating',
    ];

    /**
     * Get Attendance Rating Attribute
     *
     * @return string
     */
    public function getAttendanceRatingSymbolAttribute()
    {
        return str_repeat('&#x263A; ', $this->attendance_rating);
    }

    /**
     * Get PriceRating Symbol Attribute
     *
     * @return string
     */
    public function getPriceRatingSymbolAttribute()
    {
        return str_repeat('$ ', $this->price_rating);
    }

    /**
     * Get Comfort Rating Symbol Attribute
     *
     * @return string
     */
    public function getComfortRatingSymbolAttribute()
    {
        return str_repeat('&#x26F1; ', $this->comfort_rating);
    }

    /**
     * Get Noise Rating Symbol Attribute
     *
     * @return string
     */
    public function getNoiseRatingSymbolAttribute()
    {
        return str_repeat('&#x266C; ', $this->noise_rating);
    }

    /**
     * The relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    /**
     * The relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function foods()
    {
        return $this->belongsToMany(Food::class);
    }

    /**
     * The relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function drinks()
    {
        return $this->belongsToMany(Drink::class);
    }

    /**
     * The relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function accommodations()
    {
        return $this->belongsToMany(Accommodation::class);
    }
}
