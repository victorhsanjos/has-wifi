<?php
/**
 * Address File
 *
 * PHP version 7
 *
 * @category Model
 * @package  App
 * @author   Victor Hugo <victorhsanjos@gmail.com>
 * @license  https://choosealicense.com/licenses/mit/ MIT License
 * @link     https://bitbucket.org/victorhsanjos/has-wifi/
 */

namespace App;

use App\Filters\AddressFilters;
use Illuminate\Database\Eloquent\Model;

/**
 * Address Class
 *
 * @category Model
 * @package  App
 * @author   Victor Hugo <victorhsanjos@gmail.com>
 * @license  https://choosealicense.com/licenses/mit/ MIT License
 * @link     https://bitbucket.org/victorhsanjos/has-wifi/
 */
class Address extends Model
{
    protected $fillable = [
        'establishment_id',
        'street',
        'number',
        'district',
        'complement',
        'city',
        'state',
        'country',
    ];

    /**
     * Set number attribute
     *
     * @param int $value the number
     *
     * @return void
     */
    public function setNumberAttribute($value)
    {
        $this->attributes['number'] = $value ?? 's/n';
    }

    /**
     * Get compressed attribute
     *
     * @return string
     */
    public function getCompressedAttribute()
    {
        return "{$this->street}, {$this->number} - {$this->district}, 
        {$this->city} - {$this->state}, {$this->country}";
    }

    /**
     * The relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    /**
     * The relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluations()
    {
        return $this->hasMany(Evaluation::class);
    }

    /**
     * Apply all relevant thread filters.
     *
     * @param Builder       $query   the query
     * @param ThreadFilters $filters the filters
     *
     * @return Builder
     */
    public function scopeFilter($query, AddressFilters $filters)
    {
        return $filters->apply($query);
    }
}
