<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEstablishmentRequest extends FormRequest
{
    protected $redirectRoute = 'establishments.create';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type_id' => 'required|exists:establishment_types,id',
            'street' => 'required',
            'number' => 'required',
            'complement' => 'nullable',
            'district' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'type_id' => 'type'
        ];
    }
}
