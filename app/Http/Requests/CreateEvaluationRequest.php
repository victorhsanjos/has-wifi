<?php

namespace App\Http\Requests;

use App\Support\Traits\EvaluationAttributes;
use App\Support\Traits\EvaluationRules;
use Illuminate\Foundation\Http\FormRequest;

class CreateEvaluationRequest extends FormRequest
{
    use EvaluationRules;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function getRedirectUrl()
    {
        $url = $this->redirector->getUrlGenerator();

        $address = $this->route()->parameter('address');

        return $url->route('evaluations.create', $address);
    }
}
