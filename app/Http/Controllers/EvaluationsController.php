<?php

namespace App\Http\Controllers;

use App\Accommodation;
use App\Address;
use App\Drink;
use App\Evaluation;
use App\Food;
use App\Http\Requests\CreateEvaluationRequest;
use App\Http\Requests\UpdateEvaluationRequest;
use Illuminate\Http\Request;

class EvaluationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Address $address)
    {
        return view('evaluations.create', [
            'address' => $address,
            'foods' => Food::all(),
            'drinks' => Drink::all(),
            'accommodations' => Accommodation::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEvaluationRequest $request, Address $address)
    {
        $evaluation = new Evaluation(array_add($request->all(), 'user_id', $request->user()->id));

        $address->evaluations()->save($evaluation);

        $evaluation = $evaluation->refresh();

        $evaluation->foods()->attach($request->input('foods.*'));
        $evaluation->drinks()->attach($request->input('drinks.*'));
        $evaluation->accommodations()->attach($request->input('accommodations.*'));

        return redirect()->route('addresses.show', $address);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address, Evaluation $evaluation)
    {
        return view('evaluations.show', ['address' => $address, 'evaluation' => $evaluation]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address, Evaluation $evaluation)
    {
        $this->authorize('update-evaluation', $evaluation);

        return view('evaluations.edit', [
            'address' => $address,
            'evaluation' => $evaluation,
            'foods' => Food::all(),
            'drinks' => Drink::all(),
            'accommodations' => Accommodation::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEvaluationRequest $request, Address $address, Evaluation $evaluation)
    {
        $this->authorize('update-evaluation', $evaluation);

        $evaluation->fill(array_add($request->all(), 'user_id', $request->user()->id));
        $evaluation->save();

        $evaluation->foods()->sync($request->input('foods.*'));
        $evaluation->drinks()->sync($request->input('drinks.*'));
        $evaluation->accommodations()->sync($request->input('accommodations.*'));

        return redirect()->route('evaluations.show', [$address, $evaluation]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evaluation $evaluation)
    {
        //
    }
}
