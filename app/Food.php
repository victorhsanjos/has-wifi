<?php
/**
 * Food File
 *
 * PHP version 7
 *
 * @category Model
 * @package  App
 * @author   Victor Hugo <victorhsanjos@gmail.com>
 * @license  https://choosealicense.com/licenses/mit/ MIT License
 * @link     https://bitbucket.org/victorhsanjos/has-wifi/
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Food Class
 *
 * @category Model
 * @package  App
 * @author   Victor Hugo <victorhsanjos@gmail.com>
 * @license  https://choosealicense.com/licenses/mit/ MIT License
 * @link     https://bitbucket.org/victorhsanjos/has-wifi/
 */
class Food extends Model
{
    protected $fillable = [
        'name',
    ];
}
