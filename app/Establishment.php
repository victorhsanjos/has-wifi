<?php
/**
 * Establishment File
 *
 * PHP version 7
 *
 * @category Model
 * @package  App
 * @author   Victor Hugo <victorhsanjos@gmail.com>
 * @license  https://choosealicense.com/licenses/mit/ MIT License
 * @link     https://bitbucket.org/victorhsanjos/has-wifi/
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Establishment Class
 *
 * @category Model
 * @package  App
 * @author   Victor Hugo <victorhsanjos@gmail.com>
 * @license  https://choosealicense.com/licenses/mit/ MIT License
 * @link     https://bitbucket.org/victorhsanjos/has-wifi/
 */
class Establishment extends Model
{
    protected $fillable = [
        'name',
        'type_id',
    ];

    /**
     * The relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    /**
     * The relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type()
    {
        return $this->belongsTo(EstablishmentType::class);
    }

    /**
     * Save address information
     *
     * @param Address $address the address
     *
     * @return void
     */
    public function saveAddress(Address $address)
    {
        if (!$address->exists && !$this->addresses->contains($address)) {
            $this->addresses()->save($address);
        }
    }
}
