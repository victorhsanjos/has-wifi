<?php

namespace App\Filters;

use App\Address;

class AddressFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['establishment'];

    /**
     * Filter the query by a given username.
     *
     * @param  string $username
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function establishment($name)
    {
        return $this->builder
            ->join('establishments', 'addresses.establishment_id', '=', 'establishments.id')
            ->where('establishments.name', 'like', "$name%");
    }
}