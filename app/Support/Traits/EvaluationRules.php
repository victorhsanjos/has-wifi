<?php

namespace App\Support\Traits;

trait EvaluationRules
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'foods' => 'exists:foods,id',
            'drinks' => 'exists:drinks,id',
            'has_internet'=> 'boolean',
            'internet_speed'=> 'required_if:has_internet,1',
            'is_open_internet'=> 'boolean',
            'attendance_rating'=> 'required|integer|between:1,5',
            'price_rating'=> 'required|integer|between:1,5',
            'accommodations' => 'exists:accommodations,id',
            'comfort_rating'=> 'required|integer|between:1,5',
            'noise_rating'=> 'required|integer|between:1,5',
            'overall_rating'=> 'required|integer|between:1,5',
        ];
    }

    public function attributes()
    {
        return [
            'has_internet' => 'has internet',
            'internet_speed' => 'internet speed',
            'is_open_internet' => ' is open internet',
            'attendance_rating' => 'attendance rating',
            'price_rating' => 'price rating',
            'comfort_rating' => 'comfort rating',
            'noise_rating' => 'noise rating',
            'overall_rating' => 'overall rating',
        ];
    }

    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->after(function($validator) {
            if ($this->isInternetPasswordRequired()) {
                $validator->errors()->add('internet_password', 'The internet password field is required.');
            }
        });

        return $validator;
    }

    private function isInternetPasswordRequired()
    {
        return $this->input('has_internet') && !$this->input('is_open_internet');
    }
}