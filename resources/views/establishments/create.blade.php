@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Novo Estabelecimento</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('establishments.store') }}">
                            {{ csrf_field() }}
                            <fieldset>
                                <legend>Estabelecimento:</legend>
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-1 control-label">Nome:</label>
                                    <div class="col-md-4">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('type_id') ? ' has-error' : '' }}">
                                    <label class="col-md-1 control-label">Tipo</label>
                                    <div class="col-md-4">
                                        <select class="form-control " name="type_id" id="type">
                                            @foreach($types as $type)
                                                <option value="{{ $type->id }}" {{ (collect(old('type_id'))->contains($type->id)) ? 'selected':'' }}>{{ $type->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('type') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Endereço</legend>
                                <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-1 control-label">Rua:</label>
                                    <div class="col-md-4">
                                        <input id="name" type="text" class="form-control" name="street" value="{{ old('street') }}">

                                        @if ($errors->has('street'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('street') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <label for="name" class="col-md-2 control-label">Número:</label>
                                    <div class="col-md-2">
                                        <input id="name" type="text" class="form-control" name="number" value="{{ old('number') }}">

                                        @if ($errors->has('number'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-1 control-label">Bairro:</label>
                                    <div class="col-md-4">
                                        <input id="name" type="text" class="form-control" name="district" value="{{ old('district') }}">

                                        @if ($errors->has('district'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('district') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <label for="name" class="col-md-2 control-label">Complemento:</label>
                                    <div class="col-md-4">
                                        <input id="name" type="text" class="form-control" name="complement" value="{{ old('complement') }}">

                                        @if ($errors->has('complement'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('complement') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-1 control-label">Cidade:</label>
                                    <div class="col-md-4">
                                        <input id="name" type="text" class="form-control" name="city" value="{{ old('city') }}">

                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <label for="name" class="col-md-2 control-label">Estado:</label>
                                    <div class="col-md-4">
                                        <input id="name" type="text" class="form-control" name="state" value="{{ old('state') }}">

                                        @if ($errors->has('state'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-1 control-label">País:</label>
                                    <div class="col-md-4">
                                        <input id="name" type="text" class="form-control" name="country" value="{{ old('country') }}">

                                        @if ($errors->has('country'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-group">
                                <div class="col-md-offset-1">
                                    <button type="submit" class="btn btn-default">Cadastrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection