@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Nova Avaliação</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('evaluations.store', $address) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="address_id" value="{{ $address->id }}">
                            <fieldset>
                                <legend>Internet:</legend>
                                <div class="form-group{{ $errors->has('has_internet') ? ' has-error' : '' }}">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <input type="hidden" name="has_internet" value="0">
                                                <input type="checkbox" name="has_internet" value="1" @if(old('has_internet') ==  1) checked="checked" @endif> Tem?
                                            </label>
                                        </div>

                                        @if ($errors->has('has_internet'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('has_internet') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('internet_speed') ? ' has-error' : '' }}">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Velocidade</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="internet_speed" id="inputPassword3" value="{{ old('internet_speed') }}">

                                        @if ($errors->has('internet_speed'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('internet_speed') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('is_open_internet') ? ' has-error' : '' }}">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <input type="hidden" name="is_open_internet" value="0">
                                                <input type="checkbox" name="is_open_internet" value="1" @if(old('is_open_internet') ==  1) checked="checked" @endif> É aberta?
                                            </label>
                                        </div>

                                        @if ($errors->has('is_open_internet'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('is_open_internet') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('internet_password') ? ' has-error' : '' }}">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Senha</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="internet_password" id="inputPassword3" value="{{ old('internet_password') }}">

                                        @if ($errors->has('internet_password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('internet_password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Comida:</legend>
                                <div class="form-group{{ $errors->has('foods') ? ' has-error' : '' }}">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <select class="form-control multiple-select2" name="foods[]" id="foods" multiple="multiple">
                                            @foreach($foods as $food)
                                                <option value="{{ $food->id }}" {{ (collect(old('foods'))->contains($food->id)) ? 'selected':'' }}>{{ $food->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('foods'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('foods') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Bebida:</legend>
                                <div class="form-group{{ $errors->has('drinks') ? ' has-error' : '' }}">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <select class="form-control " name="drinks[]" id="drinks" multiple="multiple">
                                            @foreach($drinks as $drink)
                                                <option value="{{ $drink->id }}" {{ (collect(old('drinks'))->contains($drink->id)) ? 'selected':'' }}>{{ $drink->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('drinks'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('drinks') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Atendimento:</legend>
                                <div class="form-group{{ $errors->has('attendance_rating') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Nota:</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="attendance_rating" id="inlineRadio1" value="1"
                                               @if(old('attendance_rating') == 1) checked="checked" @endif> 1
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="attendance_rating" id="inlineRadio2" value="2"
                                               @if(old('attendance_rating') == 2) checked="checked" @endif> 2
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="attendance_rating" id="inlineRadio3" value="3"
                                               @if(old('attendance_rating') == 3) checked="checked" @endif> 3
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="attendance_rating" id="inlineRadio2" value="4"
                                               @if(old('attendance_rating') == 4) checked="checked" @endif> 4
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="attendance_rating" id="inlineRadio3" value="5"
                                               @if(old('attendance_rating') == 5) checked="checked" @endif> 5
                                    </label>

                                    @if ($errors->has('attendance_rating'))
                                        <div class="col-sm-offset-2">
                                            <span class="help-block">
                                                <strong>{{ $errors->first('attendance_rating') }}</strong>
                                            </span>
                                        </div>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Preço:</legend>
                                <div class="form-group{{ $errors->has('price_rating') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Nota:</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="price_rating" id="inlineRadio1" value="1"
                                               @if(old('price_rating') == 1) checked="checked" @endif> 1
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="price_rating" id="inlineRadio2" value="2"
                                               @if(old('price_rating') == 2) checked="checked" @endif> 2
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="price_rating" id="inlineRadio3" value="3"
                                               @if(old('price_rating') == 3) checked="checked" @endif> 3
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="price_rating" id="inlineRadio2" value="4"
                                               @if(old('price_rating') == 4) checked="checked" @endif> 4
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="price_rating" id="inlineRadio3" value="5"
                                               @if(old('price_rating') == 5) checked="checked" @endif> 5
                                    </label>

                                    @if ($errors->has('price_rating'))
                                        <div class="col-sm-offset-2">
                                            <span class="help-block">
                                                <strong>{{ $errors->first('price_rating') }}</strong>
                                            </span>
                                        </div>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Conforto:</legend>
                                <div class="form-group{{ $errors->has('comfort_rating') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Nota:</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="comfort_rating" id="inlineRadio1" value="1"
                                               @if(old('comfort_rating') == 1) checked="checked" @endif> 1
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="comfort_rating" id="inlineRadio2" value="2"
                                               @if(old('comfort_rating') == 2) checked="checked" @endif> 2
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="comfort_rating" id="inlineRadio3" value="3"
                                               @if(old('comfort_rating') == 3) checked="checked" @endif> 3
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="comfort_rating" id="inlineRadio2" value="4"
                                               @if(old('comfort_rating') == 4) checked="checked" @endif> 4
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="comfort_rating" id="inlineRadio3" value="5"
                                               @if(old('comfort_rating') == 5) checked="checked" @endif> 5
                                    </label>

                                    @if ($errors->has('comfort_rating'))
                                        <div class="col-sm-offset-2">
                                            <span class="help-block">
                                                <strong>{{ $errors->first('comfort_rating') }}</strong>
                                            </span>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('accommodations') ? ' has-error' : '' }}">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <select class="form-control " name="accommodations[]" id="accommodations" multiple="multiple">
                                            @foreach($accommodations as $accommodation)
                                                <option value="{{ $accommodation->id }}" {{ (collect(old('accommodations'))->contains($accommodation->id)) ? 'selected':'' }}>{{ $accommodation->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('accommodations'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('accommodations') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Ruído (Barulho):</legend>
                                <div class="form-group{{ $errors->has('noise_rating') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Nota:</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="noise_rating" id="inlineRadio1" value="1"
                                               @if(old('noise_rating') == 1) checked="checked" @endif> 1
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="noise_rating" id="inlineRadio2" value="2"
                                               @if(old('noise_rating') == 2) checked="checked" @endif> 2
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="noise_rating" id="inlineRadio3" value="3"
                                               @if(old('noise_rating') == 3) checked="checked" @endif> 3
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="noise_rating" id="inlineRadio2" value="4"
                                               @if(old('comfort_rating') == 4) checked="checked" @endif> 4
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="noise_rating" id="inlineRadio3" value="5"
                                               @if(old('noise_rating') == 5) checked="checked" @endif> 5
                                    </label>

                                    @if ($errors->has('noise_rating'))
                                        <div class="col-sm-offset-2">
                                            <span class="help-block">
                                                <strong>{{ $errors->first('noise_rating') }}</strong>
                                            </span>
                                        </div>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Avaliação geral:</legend>
                                <div class="form-group{{ $errors->has('overall_rating') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Nota:</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="overall_rating" id="inlineRadio1" value="1"
                                               @if(old('overall_rating') == 1) checked="checked" @endif> 1
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="overall_rating" id="inlineRadio2" value="2"
                                               @if(old('overall_rating') == 2) checked="checked" @endif> 2
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="overall_rating" id="inlineRadio3" value="3"
                                               @if(old('overall_rating') == 3) checked="checked" @endif> 3
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="overall_rating" id="inlineRadio2" value="4"
                                               @if(old('overall_rating') == 4) checked="checked" @endif> 4
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="overall_rating" id="inlineRadio3" value="5"
                                               @if(old('overall_rating') == 5) checked="checked" @endif> 5
                                    </label>

                                    @if ($errors->has('overall_rating'))
                                        <div class="col-sm-offset-2">
                                            <span class="help-block">
                                                <strong>{{ $errors->first('overall_rating') }}</strong>
                                            </span>
                                        </div>
                                    @endif
                                </div>
                            </fieldset>

                            <div class="form-group">
                                <div class="col-sm-offset-5">
                                    <button type="submit" class="btn btn-default">Enviar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#foods').select2({
                theme: "bootstrap"
            });

            $('#drinks').select2({
                theme: "bootstrap"
            });

            $('#accommodations').select2({
                theme: "bootstrap"
            });
        });
</script>
@endpush