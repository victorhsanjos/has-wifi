@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="jumbotron">
                    <h1>{{ $address->establishment->name }}</h1>
                    <h3>{{ $address->street }}, {{ $address->number }} - {{ $address->district }}</h3>
                    <h3>{{ $address->city }} - {{ $address->state }} - {{ $address->country }}</h3>
                    @empty (!$address->complement)
                        <h3>{{ $address->complement }}</h3>
                    @endempty
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Avaliação Geral - {{ $evaluation->overall_rating }}</h2>
                        <h3>{{ $evaluation->user->name }}</h3>
                    </div>

                    <div class="panel-body">
                        @if($evaluation->has_internet)
                            <h3>Internet</h3>
                            <p>Velocidade: {{ $evaluation->internet_speed }}</p>
                            @if ($evaluation->is_open_internet)
                                <p>Rede Aberta</p>
                            @else
                                <p>Senha: {{ $evaluation->internet_password }}</p>
                            @endif
                            <hr>
                        @endif

                        @if($evaluation->foods->isNotEmpty())
                            <h3>Comidas</h3>
                            <ul>
                                @foreach($evaluation->foods as $food)
                                    <li>{{ $food->name }}</li>
                                @endforeach
                            </ul>
                            <hr>
                        @endif

                        @if($evaluation->drinks->isNotEmpty())
                            <h3>Bebidas</h3>
                            <ul>
                                @foreach($evaluation->drinks as $drink)
                                    <li>{{ $drink->name }}</li>
                                @endforeach
                            </ul>
                            <hr>
                        @endif

                            <h3>Atendimento</h3>
                            <span title="{{ $evaluation->attendance_rating }}">{{ $evaluation->attendance_rating_symbol }}</span>
                            <hr>

                            <h3>Preço</h3>
                            <span title="{{ $evaluation->price_rating }}">{{ $evaluation->price_rating_symbol }}</span>
                            <hr>

                            <h3>Conforto</h3>
                            <span title="{{ $evaluation->comfort_rating }}">{{ $evaluation->comfort_rating_symbol }}</span>
                            <hr>

                            @if($evaluation->accommodations->isNotEmpty())
                                <h3>Acomodações</h3>
                                <ul>
                                    @foreach($evaluation->accommodations as $accommodation)
                                        <li>{{ $accommodation->name }}</li>
                                    @endforeach
                                </ul>
                                <hr>
                            @endif

                            <h3>Ruído (Barulho)</h3>
                            <span title="{{ $evaluation->noise_rating }}">{{ $evaluation->noise_rating_symbol }}</span>
                            <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection