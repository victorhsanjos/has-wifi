@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Minhas Avaliações</div>

                    <div class="panel-body">
                        <div class="form-group">
                            <form method="GET" action="{{ route('addresses.index') }}">
                                <div class="input-group">
                                    <input class="form-control" type="text" name="establishment" placeholder="Pesquise o establecimento" value="{{ request()->input('establishment') }}" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"><span style="margin-left:10px;">Buscar</span></button>
                                    </span>
                                    </span>
                                </div>
                            </form>
                        </div>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Estabelecimento</th>
                                    <th>Tipo</th>
                                    <th>Endereço</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse($addresses as $address)
                                <tr>
                                    <th scope="row">{{ $address->id }}</th>
                                    <td>{{ $address->establishment->name }}</td>
                                    <td>{{ $address->establishment->type->name }}</td>
                                    <td>{{ $address->compressed }}</td>
                                    <td>
                                        @if ($address->evaluations->isNotEmpty())
                                            <a class="btn btn-default"
                                                href="{{ route('addresses.show', $address) }}"
                                                role="button">
                                                Avaliações
                                            </a>
                                        @else
                                            <a class="btn btn-primary"
                                               href="{{ route('evaluations.create', $address) }}"
                                               role="button">
                                                Criar uma Nova Avaliação
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">Nenhum registro cadastrado!</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        {{ $addresses->appends(request()->except('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection