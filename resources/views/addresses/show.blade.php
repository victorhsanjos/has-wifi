@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <h1>{{ $address->establishment->name }}</h1>
                    <h3>{{ $address->street }}, {{ $address->number }} - {{ $address->district }}</h3>
                    <h3>{{ $address->city }} - {{ $address->state }} - {{ $address->country }}</h3>
                    @empty (!$address->complement)
                        <h3>{{ $address->complement }}</h3>
                    @endempty
                    <br>
                    <a class="btn btn-primary" href="{{ route('evaluations.create', $address) }}" role="button">Nova Avaliação</a>
                </div>

                @forelse($address->evaluations as $evaluation)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ $evaluation->user->name }}</h3>
                        </div>
                        <div class="panel-body">
                            <h2>Avaliação Geral: <strong>{{ $evaluation->overall_rating }}</strong></h2>
                            <h2>{!! $evaluation->has_internet ? 'Com Internet' : '<strike>Sem Internet</strike>' !!}</h2>
                        </div>
                        <div class="panel-footer">
                            <a class="btn btn-success btn-sm" href="{{ route('evaluations.show', [$address, $evaluation]) }}" role="button">Vizualizar</a>
                            @can('update-evaluation', $evaluation)
                                <a class="btn btn-warning btn-sm" href="{{ route('evaluations.edit', [$address, $evaluation]) }}" role="button">Alterar</a>
                            @endcan
                        </div>
                    </div>
                @empty
                    <div class="panel panel-default">
                        <div class="panel-body">
                            Nenhuma avaliação encontrada!
                        </div>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection