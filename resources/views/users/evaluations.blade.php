@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @forelse($evaluations as $evaluation)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{ $evaluation->address->establishment->name }} - {{ $evaluation->address->compressed }}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <h2>Avaliação Geral: <strong>{{ $evaluation->overall_rating }}</strong></h2>
                            <h2>{!! $evaluation->has_internet ? 'Com Internet' : '<strike>Sem Internet</strike>' !!}</h2>
                        </div>
                        <div class="panel-footer">
                            <a class="btn btn-success btn-sm" href="{{ route('evaluations.show', [$evaluation->address, $evaluation]) }}" role="button">Vizualizar</a>
                            @can('update-evaluation', $evaluation)
                                <a class="btn btn-warning btn-sm" href="{{ route('evaluations.edit', [$evaluation->address, $evaluation]) }}" role="button">Alterar</a>
                            @endcan
                        </div>
                    </div>
                @empty
                    <div class="panel panel-default">
                        <div class="panel-body">
                            Nenhuma avaliação encontrada!
                        </div>
                    </div>
                @endforelse

                {{ $evaluations->links() }}
            </div>
        </div>
    </div>
@endsection